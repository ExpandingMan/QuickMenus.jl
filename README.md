# QuickMenus

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/QuickMenus.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/QuickMenus.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/QuickMenus.jl/-/pipelines)

Simple interactive selection of items from Julia iterables.  Especially usefule for cases where you
want to select an item from a long list which may be truncated on the screen, or for picking from a
list you don't remember without having to `show` it again.

Uses Julia's [`REPL.TerminalMenus`](https://docs.julialang.org/en/v1/stdlib/REPL/#TerminalMenus)
and [`fzf`](https://github.com/junegunn/fzf).

![QuickMenus demo](/docs/quickmenus_demo.gif)
