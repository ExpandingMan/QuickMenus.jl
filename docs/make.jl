using QuickMenus
using Documenter

DocMeta.setdocmeta!(QuickMenus, :DocTestSetup, :(using QuickMenus); recursive=true)

makedocs(;
    modules=[QuickMenus],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/QuickMenus.jl/blob/{commit}{path}#{line}",
    sitename="QuickMenus.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/QuickMenus.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
