```@meta
CurrentModule = QuickMenus
```

# QuickMenus
This package provides convenient functionality for cases in which the user frequently has to refer
to lists to select function arguments, particularly in REPL sessions.

This package is a convenience wrapper for the [TerminalMenus](https://docs.julialang.org/en/v1/stdlib/REPL/#TerminalMenus)
module of the `REPL` stdlib and [`fzf`](https://github.com/junegunn/fzf).

`QuickMenus` functions can easily be composed with other functionality analogously to how `fzf` can
be easily composed with normal shell functionality.

## Example
```julia
v = [0, π/2, π, 3π/2, 2π]

sin(qm(v))  # brings up a menu to select a single element of v

# can also use pipe syntax
sin.(v |> qmm)  # brings up a menu to select any number of elements of v
```

The use case that inspired this package is selecting files.  One can of course use `qm` or `qmm` as
above but there is also `fzf` and `fzfm`.
```julia
# bring up an fzf menu for fuzzy search selecting files from current directory to read
read(fzf(readdir()))

# bring up an fzf menu for fuzzy selecting arbitrarily many files to delete
rm.(fzfm(readdir()))
```

Note that all `qm`, `qmm`, `fzf`, `fzfm` do is select elements from a list, so you can use them
anywhere, in arbitrarily complicated functions, as long as you are running and interactive session.

## API
```@docs
quickmenu
quickmulti
fzfmenu
fzfmulti
```

### Aliases
Since this package is for interactive use, it provides some very short aliases.
```@docs
qm
qmm
fzf
fzfm
```
