module QuickMenus

using REPL.TerminalMenus
using fzf_jll

using REPL.TerminalMenus: AbstractMenu

export quickmenu, quickmulti, fzfmenu, fzfmulti, filepicker, qm, qmm, fzf, fzfm, fp


"""
    quickmenu([indexer, ℳ,] itr;
              message="", cursor=1, charset=:unicode, kw...)

Presents the user with a terminal menu for selecting elements from the
iterable `itr`.

The aliases `qm` (for single selection) or `qmm` for multiple selections
are provided by `QuickMenus`.

## Arguments
- `itr`: the iterable to select elements from.
- `indexer`: Function used for indexing the iterable `itr`. `getindex`
    by default
- `ℳ`: the type of terminal menu, `RadioMenu` or `MultiSelectMenu`.
- `message`: message to display before the selection menu.
- `cursor`: Initial cursor position.
- `charset`: The character set to use for the terminal menu.
- `kw`: additional keyword arguments are provided to the terminal menu
    constructor `ℳ`.
"""
function quickmenu(indexer, ::Type{ℳ}, itr;
                   message::AbstractString="", cursor=1,
                   charset=:unicode, kw...) where {ℳ<:AbstractMenu}
    strs = [sprint(show, x) for x ∈ itr]  # must always be a Vector
    j = request(message, ℳ(strs; charset, kw...); cursor)
    j isa AbstractSet && (j = sort!(collect(j)))
    indexer(itr, j)
end
function quickmenu(::Type{ℳ}, itr; kw...) where {ℳ<:AbstractMenu}
    quickmenu(getindex, ℳ, itr; kw...)
end

quickmenu(itr; kw...) = quickmenu(RadioMenu, itr; kw...)

"""
    quickmulti(itr; kw...)

Alias for [`quickmenu`](@ref) with the `MultiSelectMenu`.
"""
quickmulti(itr; kw...) = quickmenu(MultiSelectMenu, itr; kw...)

"""
    fzfmenu([indexer,] itr[; prompt=nothing])

Select an element from `itr` using [`fzf`](https://github.com/junegunn/fzf)
fuzzy search.  Note that `fzf` is run with `--layout=reverse` so that the list
is presented in the same order as it exists in memory.
"""
function fzfmenu(indexer, itr; prompt=nothing)
    strs = [sprint(show, x) for x ∈ itr]  # must always be a Vector
    cmd = isnothing(prompt) ? `$(fzf_jll.fzf()) --layout=reverse` : `$(fzf_jll.fzf()) --layout=reverse --prompt=$prompt`
    cmd = Cmd(cmd, ignorestatus=true)
    cmd = pipeline(cmd, stdin=IOBuffer(join(strs, "\n")))
    o = String(readchomp(cmd))
    isempty(o) && return indexer(itr, Int[])
    # we assume that constructing a hash table usually isn't worth it here
    j = findfirst(==(o), strs)
    if isnothing(j)  # should be unreachable
        error("fzf returned $o but it could not be found in the iterable")
    end
    indexer(itr, j)
end
fzfmenu(itr; prompt=nothing) = fzfmenu(getindex, itr; prompt)

"""
    fzfmenu([indexer,] itr[; prompt=nothing])

Select arbitrarily many elements from `itr` using [`fzf`](https://github.com/junegunn/fzf)
fuzzy search.  note that `fzf` is run with `--layout=reverse` so that the list is presented
in the same order it exists in memory.
"""
function fzfmulti(indexer, itr; prompt=nothing)
    strs = [sprint(show, x) for x ∈ itr]  # must always be a Vector
    cmd = isnothing(prompt) ? `$(fzf_jll.fzf()) --layout=reverse -m` : `$(fzf_jll.fzf()) --layout=reverse --prompt=$prompt -m`
    cmd = Cmd(cmd, ignorestatus=true)
    cmd = pipeline(cmd, stdin=IOBuffer(join(strs, "\n")))
    o = String(readchomp(cmd))
    isempty(o) && return indexer(itr, Int[])
    o = split(o, "\n")
    # we assume that constructing a hash table usually isn't worth it here
    j = sort!([findfirst(==(ω), strs) for ω ∈ o])
    indexer(itr, j)
end
fzfmulti(itr; prompt=nothing) = fzfmulti(getindex, itr; prompt)

"""
    filepicker([path=pwd()]; done="--DONE--")

Interactively build a path using [`fzf`](https://github.com/junegunn/fzf).
If a file is selected, it will return immediately.
Select `..` to move to the parent directory.
If a directory is selected, `filepicker` will be called recurssively on that directory;
choose `--DONE--` to return the directory, or continue selecting.
"""
function filepicker(path=pwd(); done="--DONE--")
    path = abspath(path)
    isfile(path) && return path
    resp = path == "/" ? [[done]; readdir("/")] : [[done, ".."]; readdir(path)]
    resp = fzf(resp; prompt=path)
    
    resp == done ? path : filepicker(normpath(joinpath(path, resp)))
 end


"""
    qm

Alias for [`quickmenu`](@ref) with `RadioMenu`.
"""
const qm = quickmenu

"""
    qmm

Alias for [`quickmenu`](@ref) with `MultiSelectMenu`.
"""
const qmm = quickmulti

"""
    fzf

Alias for [`fzfmenu`](@ref).
"""
const fzf = fzfmenu

"""
    fzfm

Alias for [`fzfmulti`](@ref).
"""
const fzfm = fzfmulti

"""
    fp

Alias for [`filepicker`](@ref).
"""
const fp = filepicker

end
