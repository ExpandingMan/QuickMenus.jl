using QuickMenus
using Test

#====================================================================================================
       TODO:

       It would be nice to have tests but the interactive nature of this makes it pretty
       annoying.  We keep the `runtests.jl` because at least it will ensure the package
       compiles correctly.

       See:
        https://github.com/JuliaDebug/TerminalRegressionTests.jl
====================================================================================================#

@testset "QuickMenus.jl" begin
end
